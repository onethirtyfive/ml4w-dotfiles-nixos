{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = inputs@{ self, nixpkgs, nixpkgs-unstable, ... }:
    let
      forAllSystems = nixpkgs.lib.genAttrs nixpkgs.lib.systems.flakeExposed;
    in {
      overlays = {
        default = import ./overlay.nix;
      };

      packages = forAllSystems (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          ml4wPkg = import ./modules { inherit (pkgs) stdenv; };
        in {
          default = ml4wPkg;
          ml4w = ml4wPkg;
        }
      );

      defaultPackage = forAllSystems (system: self.packages.${system}.default);
    };
}
