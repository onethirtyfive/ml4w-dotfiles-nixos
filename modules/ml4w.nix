{ pkgs, config, lib, wallpapers, ... }:
let
  inherit (lib) types;
  cfg = config.ml4w;
in {
  imports = [
    ./desktop-portals.nix
    ./hyprland
    ./rofi.nix
    ./waybar
  ];

  options = {
    ml4w = {
      enable = lib.mkEnableOption "ML4W";

      scripts = lib.mkOption {
        type = types.attrs;
        description = "ML4W support scripts. Ignore unless debugging.";
        default = import ./scripts { inherit config pkgs; };
      };

      wallpapers = lib.mkOption {
        type = types.package;
        description = "ML4W companion wallpaper package.";
        default = wallpapers;
      };
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages =
      builtins.attrValues cfg.scripts; # all scripts in PATH!
  };
}
