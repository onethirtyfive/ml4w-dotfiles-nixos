#     _    _ _  __ _             _
#    / \  | | |/ _| | ___   __ _| |_
#   / _ \ | | | |_| |/ _ \ / _` | __|
#  / ___ \| | |  _| | (_) | (_| | |_
# /_/   \_\_|_|_| |_|\___/ \__,_|\__|
#
# by Stephan Raabe (2023)
# -----------------------------------------------------
{ pkgs, hyprland }:
pkgs.writeShellApplication {
  name = "ml4w-toggleallfloat.sh";

  runtimeInputs = [ hyprland ];

  text = ''
    hyprctl dispatch workspaceopt allfloat
  '';
}

