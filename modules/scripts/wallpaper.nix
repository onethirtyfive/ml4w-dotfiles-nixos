#                _ _
# __      ____ _| | |_ __   __ _ _ __   ___ _ __
# \ \ /\ / / _` | | | '_ \ / _` | '_ \ / _ \ '__|
#  \ V  V / (_| | | | |_) | (_| | |_) |  __/ |
#   \_/\_/ \__,_|_|_| .__/ \__,_| .__/ \___|_|
#                   |_|         |_|
#
# by Stephan Raabe (2023)
# -----------------------------------------------------
{ config, pkgs, coreutils, findutils, gnused, jq, libnotify, pywal, rofi, swww, ... }:
let
  configdir = config.xdg.configHome;
  cachedir = config.xdg.cacheHome;

  waybarConfigdir = "${configdir}/waybar";
  walCachedir = "${cachedir}/wal";
  wallpapers = config.ml4w.wallpapers;

  rofiConfig = pkgs.writeText "config-wallpaper.rasi" ''
    /* ---- Configuration ---- */
    configuration {
        modi:                       "drun,run";
        font:                       "Fira Sans Bold 10";
        show-icons:                 true;
        hover-select:               true;
        me-select-entry:            "";
        me-accept-entry:            "MousePrimary";
        drun-display-format:        "{name}";
        window-format:              "{w} · {c} · {t}";

    }

    /* ---- Load pywal colors (custom wal template) ---- */
    @import "${cachedir}/wal/colors-rofi-pywal.rasi"
    @import "${cachedir}/current_wallpaper.rasi"

    /* ---- Window ---- */
    window {
        width:                        761px;
        x-offset:                     0px;
        y-offset:                     65px;
        spacing:                      0px;
        padding:                      0px;
        margin:                       0px;
        color:                        #FFFFFF;
        border:                       3px;
        border-color:                 #FFFFFF;
        cursor:                       "default";
        transparency:                 "real";
        location:                     north;
        anchor:                       north;
        fullscreen:                   false;
        enabled:                      true;
        border-radius:                10px;
        background-color:             transparent;
    }

    /* ---- Mainbox ---- */
    mainbox {
        enabled:                      true;
        orientation:                  horizontal;
        spacing:                      0px;
        margin:                       0px;
        background-color:             @background;
        children:                     ["listbox"];
    }

    /* ---- Imagebox ---- */
    imagebox {
        padding:                      18px;
        background-color:             transparent;
        orientation:                  vertical;
        children:                     [ "inputbar", "dummy", "mode-switcher" ];
    }

    /* ---- Listbox ---- */
    listbox {
        spacing:                     20px;
        background-color:            transparent;
        orientation:                 vertical;
        children:                    [ "inputbar", "message", "listview" ];
    }

    /* ---- Dummy ---- */
    dummy {
        background-color:            transparent;
    }

    /* ---- Inputbar ---- */
    inputbar {
        enabled:                      true;
        text-color:                   @foreground;
        spacing:                      10px;
        padding:                      15px;
        border-radius:                0px;
        border-color:                 @foreground;
        background-color:             @background;
        children:                     [ "textbox-prompt-colon", "entry" ];
    }

    textbox-prompt-colon {
        enabled:                      true;
        expand:                       false;
        str:                          "";
        background-color:             transparent;
        text-color:                   inherit;
    }

    entry {
        enabled:                      true;
        background-color:             transparent;
        text-color:                   inherit;
        cursor:                       text;
        placeholder:                  "Search";
        placeholder-color:            inherit;
    }

    /* ---- Mode Switcher ---- */
    mode-switcher{
        enabled:                      true;
        spacing:                      20px;
        background-color:             transparent;
        text-color:                   @foreground;
    }

    button {
        padding:                      10px;
        border-radius:                10px;
        background-color:             @background;
        text-color:                   inherit;
        cursor:                       pointer;
        border:                       0px;
    }

    button selected {
        background-color:             @color11;
        text-color:                   @foreground;
    }

    /* ---- Listview ---- */
    listview {
        enabled:                      true;
        columns:                      1;
        cycle:                        true;
        dynamic:                      false;
        scrollbar:                    true;
        layout:                       horizontal;
        reverse:                      false;
        spacing:                      10px;
        padding:                      10px;
        margin:                       0px;
        background-color:             @background;
        border:                       0px;
    }

    /* ---- Element ---- */
    element {
        enabled:                      true;
        padding:                      0px;
        margin:                       5px;
        cursor:                       pointer;
        background-color:             @background;
        border-radius:                10px;
        border:                       2px;
        layout:                       vertical;
    }

    element normal.normal {
        border-color:                @color11;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element normal.urgent {
        border-color:                @color11;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element normal.active {
        border-color:                @color11;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element selected.normal {
        border-color:                @foreground;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element selected.urgent {
        border-color:                @foreground;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element selected.active {
        border-color:                @foreground;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.normal {
        border-color:                @color11;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.urgent {
        border-color:                @color11;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.active {
        border-color:                @color11;
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element-icon {
        background-color:            transparent;
        text-color:                  inherit;
        size:                        100px;
        cursor:                      inherit;
        horizontal-align:              0.5;
    }

    element-text {
        background-color:            transparent;
        text-color:                  inherit;
        cursor:                      inherit;
        vertical-align:              0.5;
        horizontal-align:            0.1;
        enabled: false;
    }

    /*****----- Message -----*****/
    message {
        background-color:            transparent;
        border:0px;
        margin:20px 0px 0px 0px;
        padding:0px;
        spacing:0px;
        border-radius: 10px;
    }

    textbox {
        padding:                     15px;
        margin:                      0px;
        border-radius:               0px;
        background-color:            @background;
        text-color:                  @foreground;
        vertical-align:              0.5;
        horizontal-align:            0.0;
    }

    error-message {
        padding:                     15px;
        border-radius:               20px;
        background-color:            @background;
        text-color:                  @foreground;
    }
  '';
in pkgs.writeShellApplication {
  name = "ml4w-wallpaper.sh";

  runtimeInputs = [
    config.wayland.windowManager.hyprland.finalPackage
    coreutils findutils gnused jq libnotify pywal rofi swww
  ];

  text = ''
    set +x
    set -eu

    # Cache file for holding the current wallpaper
    cache_file="${cachedir}/current_wallpaper"
    rasi_file="${cachedir}/current_wallpaper.rasi"

    # Create cache file if not exists
    if [ ! -f $cache_file ] ;then
        touch $cache_file
        echo "${wallpapers}/default.jpg" > "$cache_file"
    fi

   # Create rasi file if not exists
    if [ ! -f $rasi_file ] ;then
        touch $rasi_file
        echo "* { current-image: url(\"${wallpapers}/default.jpg\", height); }" > "$rasi_file"
    fi

    current_wallpaper=$(cat "$cache_file")

    case "''${1:-random}" in
        # Load wallpaper from .cache of last session
        "init")
            if [ -f $cache_file ]; then
                wal -q -i "$current_wallpaper"
            else
                wal -q -i "${wallpapers}"
            fi
        ;;

        # Select wallpaper with rofi
        "select")

            selected=$( find "${wallpapers}" -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" \) -exec basename {} \; | sort -R | while read -r rfile
            do
                echo -en "$rfile\x00icon\x1f${wallpapers}/''${rfile}\n"
            done | rofi -dmenu -replace -config ${rofiConfig})
            if [ ! "$selected" ]; then
                echo "No wallpaper selected"
                exit
            fi
            wal -q -i "${wallpapers}/$selected"
        ;;

        # Randomly select wallpaper
        *)
            wal -q -i "${wallpapers}"
        ;;

    esac

    # -----------------------------------------------------
    # Load current pywal color scheme
    # -----------------------------------------------------
    set +u;
    # shellcheck disable=SC1091
    source "${cachedir}/wal/colors.sh"

    # -----------------------------------------------------
    # Write selected wallpaper into .cache files
    # -----------------------------------------------------
    # shellcheck disable=SC2154
    echo "Wallpaper: $wallpaper"
    echo "$wallpaper" > "$cache_file"
    echo "* { current-image: url(\"$wallpaper\", height); }" > "$rasi_file"

    # -----------------------------------------------------
    # get wallpaper image name
    # -----------------------------------------------------

    # shellcheck disable=SC2001
    newwall=$(echo "$wallpaper" | sed "s|${wallpapers}/||g")

    # -----------------------------------------------------
    # Set the new wallpaper
    # -----------------------------------------------------
    # transition_type="outer"
    transition_type="random"

    swww img "$wallpaper" \
        --transition-bezier .43,1.19,1,.4 \
        --transition-fps=60 \
        --transition-type=$transition_type \
        --transition-duration=0.7 \
        --transition-pos "$( hyprctl cursorpos )"

    sed -n '/\/* BEGIN STYLES \*\//,$p' "${waybarConfigdir}/style.css" | cat "${walCachedir}/colors-waybar.css" - > "${waybarConfigdir}/style.css.tmp" && mv "${waybarConfigdir}/style.css.tmp" "${waybarConfigdir}/style.css"

    echo "yay"
    systemctl --user restart waybar

    # -----------------------------------------------------
    # Reload waybar with new colors
    # -----------------------------------------------------
    export HYPRLAND_INSTANCE_SIGNATURE="bogus"
    for i in $(hyprctl instances -j | jq ".[].instance" -r); do
      HYPRLAND_INSTANCE_SIGNATURE=$i hyprctl reload config-only
    done
    unset HYPRLAND_INSTANCE_SIGNATURE

    # -----------------------------------------------------
    # Send notification
    # -----------------------------------------------------
    sleep 1
    notify-send "Colors and Wallpaper updated" "with image $newwall"

    echo "DONE!"
  '';
}
