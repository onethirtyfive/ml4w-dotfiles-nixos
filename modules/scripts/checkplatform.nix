{ pkgs, coreutils, ... }:
pkgs.writeShellApplication {
  name = "ml4w-checkplatform.sh";

  runtimeInputs = [ coreutils ];

  text = ''
    #  ____  _       _    __
    # |  _ \| | __ _| |_ / _| ___  _ __ _ __ ___
    # | |_) | |/ _` | __| |_ / _ \| '__| '_ ` _ \
    # |  __/| | (_| | |_|  _| (_) | |  | | | | | |
    # |_|   |_|\__,_|\__|_|  \___/|_|  |_| |_| |_|
    #
    # by Stephan Raabe (2023)
    # -----------------------------------------------------
    # 3 = Desktop
    # 10 = Laptop
    # -----------------------------------------------------
    cat /sys/class/dmi/id/chassis_type
  '';
}

