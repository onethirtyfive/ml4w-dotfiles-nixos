#!/bin/bash
#  _              _     _           _ _
# | | _____ _   _| |__ (_)_ __   __| (_)_ __   __ _ ___
# | |/ / _ \ | | | '_ \| | '_ \ / _` | | '_ \ / _` / __|
# |   <  __/ |_| | |_) | | | | | (_| | | | | | (_| \__ \
# |_|\_\___|\__, |_.__/|_|_| |_|\__,_|_|_| |_|\__, |___/
#           |___/                             |___/
# by Stephan Raabe (2023)
# -----------------------------------------------------
{ config, pkgs, coreutils, rofi, wl-clipboard, cliphist, ... }:
let
  cachedir = config.xdg.cacheHome;

  rofiConfig = pkgs.writeText "config-compact.rasi" ''
    /* ---- Configuration ---- */
    configuration {
        modi:                       "drun,run";
        font:                       "Fira Sans Bold 10";
        show-icons:                 false;
        icon-theme:                 "kora";
        display-drun:               "APPS";
        display-run:                "RUN";
        display-filebrowser:        "FILES";
        display-window:             "WINDOW";
        hover-select:               true;
        me-select-entry:            "";
        me-accept-entry:            "MousePrimary";
        drun-display-format:        "{name}";
        window-format:              "{w} · {c} · {t}";

    }

    /* ---- Load pywal colors (custom wal template) ---- */
    @import "${cachedir}/wal/colors-rofi-pywal.rasi"
    @import "${cachedir}/current_wallpaper.rasi"

    /* ---- Window ---- */
    window {
        width:                        600px;
        x-offset:                     0px;
        y-offset:                     65px;
        spacing:                      0px;
        padding:                      0px;
        margin:                       0px;
        color:                        #FFFFFF;
        border:                       3px;
        border-color:                 #FFFFFF;
        cursor:                       "default";
        transparency:                 "real";
        location:                     north;
        anchor:                       north;
        fullscreen:                   false;
        enabled:                      true;
        border-radius:                10px;
        background-color:             transparent;
    }

    /* ---- Mainbox ---- */
    mainbox {
        enabled:                      true;
        orientation:                  horizontal;
        spacing:                      0px;
        margin:                       0px;
        background-color:             @background;
        background-image:             @current-image;
        children:                     ["listbox"];
    }

    /* ---- Imagebox ---- */
    imagebox {
        padding:                      18px;
        background-color:             transparent;
        orientation:                  vertical;
        children:                     [ "inputbar", "dummy", "mode-switcher" ];
    }

    /* ---- Listbox ---- */
    listbox {
        spacing:                     20px;
        background-color:            transparent;
        orientation:                 vertical;
        children:                    [ "inputbar", "message", "listview" ];
    }

    /* ---- Dummy ---- */
    dummy {
        background-color:            transparent;
    }

    /* ---- Inputbar ---- */
    inputbar {
        enabled:                      true;
        text-color:                   @foreground;
        spacing:                      10px;
        padding:                      15px;
        border-radius:                0px;
        border-color:                 @foreground;
        background-color:             @background;
        children:                     [ "textbox-prompt-colon", "entry" ];
    }

    textbox-prompt-colon {
        enabled:                      true;
        expand:                       false;
        str:                          "";
        background-color:             transparent;
        text-color:                   inherit;
    }

    entry {
        enabled:                      true;
        background-color:             transparent;
        text-color:                   inherit;
        cursor:                       text;
        placeholder:                  "Search";
        placeholder-color:            inherit;
    }

    /* ---- Mode Switcher ---- */
    mode-switcher{
        enabled:                      true;
        spacing:                      20px;
        background-color:             transparent;
        text-color:                   @foreground;
    }

    button {
        padding:                      10px;
        border-radius:                10px;
        background-color:             @background;
        text-color:                   inherit;
        cursor:                       pointer;
        border:                       0px;
    }

    button selected {
        background-color:             @color11;
        text-color:                   @foreground;
    }

    /* ---- Listview ---- */
    listview {
        enabled:                      true;
        columns:                      1;
        lines:                        8;
        cycle:                        true;
        dynamic:                      true;
        scrollbar:                    false;
        layout:                       vertical;
        reverse:                      false;
        fixed-height:                 true;
        fixed-columns:                true;
        spacing:                      0px;
        padding:                      10px;
        margin:                       0px;
        background-color:             @background;
        border:0px;
    }

    /* ---- Element ---- */
    element {
        enabled:                      true;
        padding:                      10px;
        margin:                       5px;
        cursor:                       pointer;
        background-color:             @background;
        border-radius:                10px;
        border:                       2px;
    }

    element normal.normal {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element normal.urgent {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element normal.active {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element selected.normal {
        background-color:            @color11;
        text-color:                  @foreground;
    }

    element selected.urgent {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element selected.active {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.normal {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.urgent {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.active {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element-icon {
        background-color:            transparent;
        text-color:                  inherit;
        size:                        32px;
        cursor:                      inherit;
    }

    element-text {
        background-color:            transparent;
        text-color:                  inherit;
        cursor:                      inherit;
        vertical-align:              0.5;
        horizontal-align:            0.0;
    }

    /*****----- Message -----*****/
    message {
        background-color:            transparent;
        border:0px;
        margin:20px 0px 0px 0px;
        padding:0px;
        spacing:0px;
        border-radius: 10px;
    }

    textbox {
        padding:                     15px;
        margin:                      0px;
        border-radius:               0px;
        background-color:            @background;
        text-color:                  @foreground;
        vertical-align:              0.5;
        horizontal-align:            0.0;
    }

    error-message {
        padding:                     15px;
        border-radius:               20px;
        background-color:            @background;
        text-color:                  @foreground;
    }
  '';
in pkgs.writeShellApplication {
  name = "ml4w-keybinds.sh";
  runtimeInputs = [ coreutils rofi cliphist wl-clipboard ];

  text = ''
    # -----------------------------------------------------
    # Get keybindings location based on variation
    # -----------------------------------------------------
    config_file=$(cat ~/dotfiles/hypr/conf/keybinding.conf)
    config_file=''${config_file/source = ~/}
    config_file=''${config_file/source=~/}

    # -----------------------------------------------------
    # Path to keybindings config file
    # -----------------------------------------------------
    config_file="/home/$USER$config_file"
    echo "Reading from: $config_file"

    # -----------------------------------------------------
    # Parse keybindings
    # -----------------------------------------------------
    keybinds=$(grep -oP '(?<=bind = ).*' "$config_file")

    # shellcheck disable=SC2016
    keybinds=$(echo "$keybinds" | sed 's/$mainMod/SUPER/g'|  sed 's/,\([^,]*\)$/ = \1/' | sed 's/, exec//g' | sed 's/^,//g')

    # -----------------------------------------------------
    # Show keybindings in rofi
    # -----------------------------------------------------
    rofi -dmenu -i -replace -p "Keybinds" -config ${rofiConfig} <<< "$keybinds"
  '';
}
