#   ____ _ _       _     _     _
#  / ___| (_)_ __ | |__ (_)___| |_
# | |   | | | '_ \| '_ \| / __| __|
# | |___| | | |_) | | | | \__ \ |_
#  \____|_|_| .__/|_| |_|_|___/\__|
#           |_|
#
# by Stephan Raabe (2023)
# -----------------------------------------------------
{ config, pkgs, coreutils, rofi, wl-clipboard, cliphist, ... }:
let
  cachedir = config.xdg.cacheHome;
  rofiThemesDir = "${config.xdg.dataHome}/rofi/themes";

  rofiConfig = pkgs.writeText "config-cliphist.rasi" ''
    /* ---- Configuration ---- */
    configuration {
        modi:                       "drun,run";
        font:                       "Fira Sans Bold 10";
        show-icons:                 false;
        icon-theme:                 "kora";
        display-drun:               "APPS";
        display-run:                "RUN";
        display-filebrowser:        "FILES";
        display-window:             "WINDOW";
        hover-select:               true;
        me-select-entry:            "";
        me-accept-entry:            "MousePrimary";
        drun-display-format:        "{name}";
        window-format:              "{w} · {c} · {t}";

    }

    /* ---- Load pywal colors (custom wal template) ---- */
    @import "${cachedir}/wal/colors-rofi-pywal"
    //@import "${cachedir}/current_wallpaper.rasi"

    /* ---- Window ---- */
    window {
        width:                        600px;
        x-offset:                     0px;
        y-offset:                     65px;
        spacing:                      0px;
        padding:                      0px;
        margin:                       0px;
        color:                        #FFFFFF;
        border:                       3px;
        border-color:                 #FFFFFF;
        cursor:                       "default";
        transparency:                 "real";
        location:                     north;
        anchor:                       north;
        fullscreen:                   false;
        enabled:                      true;
        border-radius:                10px;
        background-color:             transparent;
    }

    /* ---- Mainbox ---- */
    mainbox {
        enabled:                      true;
        orientation:                  horizontal;
        spacing:                      0px;
        margin:                       0px;
        background-color:             @background;
        background-image:             @current-image;
        children:                     ["listbox"];
    }

    /* ---- Imagebox ---- */
    imagebox {
        padding:                      18px;
        background-color:             transparent;
        orientation:                  vertical;
        children:                     [ "inputbar", "dummy", "mode-switcher" ];
    }

    /* ---- Listbox ---- */
    listbox {
        spacing:                     20px;
        background-color:            transparent;
        orientation:                 vertical;
        children:                    [ "inputbar", "message", "listview" ];
    }

    /* ---- Dummy ---- */
    dummy {
        background-color:            transparent;
    }

    /* ---- Inputbar ---- */
    inputbar {
        enabled:                      true;
        text-color:                   @foreground;
        spacing:                      10px;
        padding:                      15px;
        border-radius:                0px;
        border-color:                 @foreground;
        background-color:             @background;
        children:                     [ "textbox-prompt-colon", "entry" ];
    }

    textbox-prompt-colon {
        enabled:                      true;
        expand:                       false;
        str:                          "";
        background-color:             transparent;
        text-color:                   inherit;
    }

    entry {
        enabled:                      true;
        background-color:             transparent;
        text-color:                   inherit;
        cursor:                       text;
        placeholder:                  "Search";
        placeholder-color:            inherit;
    }

    /* ---- Mode Switcher ---- */
    mode-switcher{
        enabled:                      true;
        spacing:                      20px;
        background-color:             transparent;
        text-color:                   @foreground;
    }

    button {
        padding:                      10px;
        border-radius:                10px;
        background-color:             @background;
        text-color:                   inherit;
        cursor:                       pointer;
        border:                       0px;
    }

    button selected {
        background-color:             @color11;
        text-color:                   @foreground;
    }

    /* ---- Listview ---- */
    listview {
        enabled:                      true;
        columns:                      1;
        lines:                        8;
        cycle:                        true;
        dynamic:                      true;
        scrollbar:                    false;
        layout:                       vertical;
        reverse:                      false;
        fixed-height:                 true;
        fixed-columns:                true;
        spacing:                      0px;
        padding:                      10px;
        margin:                       0px;
        background-color:             @background;
        border:0px;
    }

    /* ---- Element ---- */
    element {
        enabled:                      true;
        padding:                      10px;
        margin:                       5px;
        cursor:                       pointer;
        background-color:             @background;
        border-radius:                10px;
        border:                       2px;
    }

    element normal.normal {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element normal.urgent {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element normal.active {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element selected.normal {
        background-color:            @color11;
        text-color:                  @foreground;
    }

    element selected.urgent {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element selected.active {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.normal {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.urgent {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element alternate.active {
        background-color:            inherit;
        text-color:                  @foreground;
    }

    element-icon {
        background-color:            transparent;
        text-color:                  inherit;
        size:                        32px;
        cursor:                      inherit;
    }

    element-text {
        background-color:            transparent;
        text-color:                  inherit;
        cursor:                      inherit;
        vertical-align:              0.5;
        horizontal-align:            0.0;
    }

    /*****----- Message -----*****/
    message {
        background-color:            transparent;
        border:0px;
        margin:20px 0px 0px 0px;
        padding:0px;
        spacing:0px;
        border-radius: 10px;
    }

    textbox {
        padding:                     15px;
        margin:                      0px;
        border-radius:               0px;
        background-color:            @background;
        text-color:                  @foreground;
        vertical-align:              0.5;
        horizontal-align:            0.0;
    }

    error-message {
        padding:                     15px;
        border-radius:               20px;
        background-color:            @background;
        text-color:                  @foreground;
    }
  '';
in pkgs.writeShellApplication {
  name = "ml4w-cliphist.sh";
  runtimeInputs = [ coreutils rofi cliphist wl-clipboard ];

  text = ''
    case $1 in
        d) cliphist list | rofi -dmenu -replace -config ${rofiConfig} | cliphist delete
           ;;

        w) dothing=$(echo -e "Clear\nCancel" | rofi -dmenu -config ${rofiConfig});
           if [ "$dothing" == "Clear" ] ; then
                  cliphist wipe
           fi
           ;;

        *) cliphist list | rofi -dmenu -replace -config ${rofiConfig} | cliphist decode | wl-copy
           ;;
    esac
  '';
}
