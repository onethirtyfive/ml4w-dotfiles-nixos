{ config, pkgs, ... }:
let
  inherit (pkgs) callPackage;
  rofi = config.programs.rofi.finalPackage; # as configured via hm
in {
  calculator = callPackage ./calculator.nix { inherit rofi; };
  checkplatform = callPackage ./checkplatform.nix { };
  cliphist = callPackage ./cliphist.nix { inherit config rofi; };
  figlet = callPackage ./figlet.nix { inherit config; };
  keybindings = callPackage ./keybindings.nix { inherit config rofi; };
  screenshot = callPackage ./screenshot.nix { inherit config rofi; };
  # callPackage ./themeswitcher.nix { })
  # callPackage ./fontsearch.nix { })
  toggleallfloat = callPackage ./toggleallfloat.nix { };
  wallpaper = callPackage ./wallpaper.nix { inherit config rofi; };
}
