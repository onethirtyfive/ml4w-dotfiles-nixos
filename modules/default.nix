{ stdenv, ... }:
stdenv.mkDerivation {
  name = "ML4W dotfiles";
  version = "0.0.1"; # TODO: version parity
  src = ./.;

  buildPhase = ''
    mkdir $out
    cp -r $src/. $out
  '';
}
