{ config, pkgs, lib, ... }:
let
  inherit (lib) types;
  cfg = config.ml4w.rofi;
in {
  options.ml4w.rofi = {
    pywalTemplate = lib.mkOption {
      type = types.str;
      description = "wal template for rofi colors. Ignore unless debugging.";
      default = ''
        * {{
            background: rgba(0,0,1,0.5);
            foreground: #FFFFFF;
            color0:     {color0};
            color1:     {color1};
            color2:     {color2};
            color3:     {color3};
            color4:     {color4};
            color5:     {color5};
            color6:     {color6};
            color7:     {color7};
            color8:     {color8};
            color9:     {color9};
            color10:    {color10};
            color11:    {color11};
            color12:    {color12};
            color13:    {color13};
            color14:    {color14};
            color15:    {color15};
        }}
      '';
    };
  };

  config = lib.mkIf config.ml4w.enable {
    home.file.".config/wal/templates/colors-rofi-pywal.rasi".text = cfg.pywalTemplate;

    programs.rofi = {
      enable = true;
      package = pkgs.rofi-wayland.override {
        plugins = with pkgs; [
          rofi-calc
          rofi-top
          # rofi-power-menu
          # rofi-emoji
          # rofi-rbw-wayland
          # rofi-file-browser
          # rofi-pulse-select
        ];
      };
    };
  };
}

