# __        __          _
# \ \      / /_ _ _   _| |__   __ _ _ __
#  \ \ /\ / / _` | | | | '_ \ / _` | '__|
#   \ V  V / (_| | |_| | |_) | (_| | |
#    \_/\_/ \__;_|\__; |_.__/ \__;_|_|
#                 |___/
#
# by Stephan Raabe (2023)
# -----------------------------------------------------
{ config, ... }:
let
  baseSettings = {
    "hyprland/workspaces" = {
      on-click = "activate";
      active-only = false;
      all-outputs = true;
      format = "{}";
      format-icons = {
        urgent = "";
        active = "";
        default = "";
      };
      persistent-workspaces = {
        "*" = 5;
      };
    };

    "wlr/taskbar" = {
      format = "{icon}";
      icon-size = 18;
      tooltip-format = "{title}";
      on-click = "activate";
      on-click-middle = "close";
      ignore-list = [
        "Alacritty"
      ];
      app_ids-mapping = {
        # firefoxdeveloperedition = "firefox-developer-edition";
      };
      rewrite = {
        "Firefox Web Browser" = "Firefox";
        "Foot Server" = "Terminal";
      };
    };

    "hyprland/window" = {
      rewrite = {
        "(.*) - Brave" = "$1";
        "(.*) - Chromium" = "$1";
        "(.*) - Brave Search" = "$1";
        "(.*) - Outlook" = "$1";
        "(.*) Microsoft Teams" = "$1";
      };
      separate-outputs = true;
    };

    "custom/cliphist" = {
      format = "";
      on-click = "sleep 0.1 && ~/dotfiles/scripts/cliphist.sh";
      on-click-right = "sleep 0.1 && ~/dotfiles/scripts/cliphist.sh d";
      on-click-middle = "sleep 0.1 && ~/dotfiles/scripts/cliphist.sh w";
      tooltip = false;
    };

    "custom/starter" = {
      format = "THEME STARTER";
      tooltip = false;
    };

    "custom/keybindings" = {
      format = "";
      on-click = "~/dotfiles/hypr/scripts/keybindings.sh";
      tooltip = false;
    };

    "custom/calculator" = {
      format = "";
      on-click = "qalculate-gtk";
      tooltip = false;
    };

    "custom/appmenu" = {
      format = "Apps";
      on-click = "rofi -show drun -replace";
      on-click-right = "~/dotfiles/hypr/scripts/keybindings.sh";
      tooltip = false;
    };

    "custom/exit" = {
      format = "";
      on-click = "wlogout";
      tooltip = false;
    };

    keyboard-state = {
      numlock = true;
      capslock = true;
      format = "{name} {icon}";
      format-icons = {
        locked = "";
        unlocked = "";
      };
    };

    tray = {
      # icon-size = 21;
      spacing = 10;
    };

    clock = {
      # timezone = "America/New_York";
      tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
      format-alt = "{:%Y-%m-%d}";
    };

    "group/hardware" = {
      orientation = "inherit";
      drawer = {
        transition-duration = 300;
        children-class = "not-memory";
        transition-left-to-right = false;
      };

      "custom/system" = {
        format = "";
        tooltip = false;
      };

      disk = {
        interval = 30;
        format = "D {percentage_used}% ";
        path = "/";
        on-click = "alacritty -e htop";
      };

      cpu = {
        format = "/ C {usage}% ";
        on-click = "alacritty -e htop";
      };

      memory = {
        format = "/ M {}% ";
        on-click = "alacritty -e htop";
      };

      "hyprland/language" = {
        format = "/ K {short}";
      };
    };

    "group/settings" = {
      orientation = "inherit";

      drawer = {
        transition-duration = 300;
        children-class = "not-memory";
        transition-left-to-right = false;
      };

      "custom/settings" = {
        format = "";
        on-click = "alacritty --class dotfiles-floating -e ~/dotfiles/hypr/settings/settings.sh";
        tooltip = false;
      };

      "custom/waybarthemes" = {
        format = "";
        on-click = "~/dotfiles/waybar/themeswitcher.sh";
        tooltip = false;
      };

      "custom/wallpaper" = {
        format = "";
        on-click = "~/dotfiles/hypr/scripts/wallpaper.sh select";
        on-click-right = "~/dotfiles/hypr/scripts/wallpaper.sh";
        tooltip = false;
      };
    };

    "group/quicklinks" = {
      orientation = "horizontal";

      "custom/filemanager" = {
        format = "";
        on-click = "thunar";
        tooltip = false;
      };

      "custom/browser" = {
        format = "";
        on-click = "firefox";
        tooltip = false;
      };
    };

    network = {
      format = "{ifname}";
      format-wifi = "   {signalStrength}%";
      format-ethernet = "  {ipaddr}";
      format-disconnected = "Not connected";  #An empty format will hide the module.
      tooltip-format = " {ifname} via {gwaddri}";
      tooltip-format-wifi = "   {essid} ({signalStrength}%)";
      tooltip-format-ethernet = "  {ifname} ({ipaddr}/{cidr})";
      tooltip-format-disconnected = "Disconnected";
      max-length = 50;
      on-click = "alacritty -e nmtui";
    };

    battery = {
      states = {
        # good = 95;
        warning = 30;
        critical = 15;
      };
      format = "{icon}   {capacity}%";
      format-charging = "  {capacity}%";
      format-plugged = "  {capacity}%";
      format-alt = "{icon}  {time}";
      # format-good = ""  # An empty format will hide the module
      # format-full = ""
      format-icons = [ " "  " "  " "  " "  " " ];
    };

    pulseaudio = {
      # scroll-step = 1; # %; can be a float
      format = "{icon} {volume}%";
      format-bluetooth = "{volume}% {icon} {format_source}";
      format-bluetooth-muted = " {icon} {format_source}";
      format-muted = " {format_source}";
      format-source = "{volume}% ";
      format-source-muted = "";
      format-icons = {
        headphone = "";
        hands-free = "";
        headset = "";
        phone = "";
        portable = "";
        car = "";
        default = [ ""  " "  " " ];
      };
      on-click = "pavucontrol";
    };

    bluetooth = {
      format-disabled = "";
      format-off = "";
      interval = 30;
      on-click = "blueman-manager";
    };

    user = {
      format = "{user}";
      interval = 60;
      icon = false;
    };
  };

  settings = {
    layer = "top";
    margin-bottom = 0;

    # Position BOTTOM
    # position = bottom;
    # margin-top = 0;
    # margin-bottom = 14;

    margin-left = 0;
    margin-right = 0;
    spacing = 0;

    modules-left = [
      "custom/appmenu"
      "custom/settings"
      "custom/waybarthemes"
      "custom/wallpaper"
      # wlr/taskbar;
      "group/quicklinks"
      "hyprland/window"
      "custom/starter"
    ];

    modules-center = [ "hyprland/workspaces" ];

    modules-right = [
      "custom/updates"
      "pulseaudio"
      "bluetooth"
      "battery"
      "network"
      "group/hardware"
      "custom/cliphist"
      "custom/exit"
      "clock"
    ];
  };
in {
  ml4w-starter =
       baseSettings
    // settings
    // {
         style = ''
           /* BEGIN STYLES */
           ${builtins.readFile ./style.css}
         '';
       };
}

