# __        __          _
# \ \      / /_ _ _   _| |__   __ _ _ __
#  \ \ /\ / / _` | | | | '_ \ / _` | '__|
#   \ V  V / (_| | |_| | |_) | (_| | |
#    \_/\_/ \__,_|\__, |_.__/ \__,_|_|
#                 |___/
#
# by Stephan Raabe (2023)
{ config, pkgs, ... }:
let
  inherit (pkgs) lib;

  cfg = config.ml4w.waybar;

  settings = {
    layer = "top";
    margin-top = 14;
    margin-bottom = 0;

    # "position" = "bottom";
    # "margin-top" = 0;
    # "margin-bottom" = 14;

    margin-left = 0;
    margin-right = 0;
    spacing = 0;

    modules-left =
         [
           "custom/appmenu"
           "custom/settings"
           "custom/waybarthemes"
           "custom/wallpaper"
         ]
      ++ lib.optionals cfg.widgets.taskbar [ "wlr/taskbar" ]
      ++ [
           "group/quicklinks"
           "hyprland/window"
         ];

    modules-center = [ "hyprland/workspaces" ];

    modules-right =
         [
           "pulseaudio"
           "battery"
         ]
      ++ lib.optionals cfg.widgets.network [ "network" ]
      ++ [
           "group/hardware"
           "custom/cliphist"
         ]
      ++ lib.optionals cfg.widgets.idle-inhibitor [ "idle_inhibitor" ]
      ++ lib.optionals cfg.widgets.tray [ "tray" ]
      ++ [
           "custom/exit"
           "clock"
         ];
  };

  vary = let
    themeStyles = builtins.readFile ./style.css;
  in name: colors: pkgs.writeText "ml4w-opaque-${name}-style.css" ''
    /* BEGIN STYLES */
    ${colors}
    ${themeStyles}
  '';

  mkVariant = name: colors: {
    settings = [ (config.ml4w.waybar.commonBaseSettings // settings) ];
    style = vary name colors;
  };
in {
  ml4w-opaque-black = mkVariant "black" ''
    @define-color backgroundlight #000000;
    @define-color backgrounddark #000000;
    @define-color workspacesbackground1 #000000;
    @define-color workspacesbackground2 #222222;
    @define-color bordercolor #000000;
    @define-color textcolor1 #FFFFFF;
    @define-color textcolor2 #FFFFFF;
    @define-color textcolor3 #000000;
    @define-color iconcolor #000000;
  '';

  ml4w-opaque-color = mkVariant "color" ''
    @define-color backgroundlight @color5;
    @define-color backgrounddark @color11;
    @define-color workspacesbackground1 @color5;
    @define-color workspacesbackground2 @color11;
    @define-color bordercolor @color11;
    @define-color textcolor1 #FFFFFF;
    @define-color textcolor2 #FFFFFF;
    @define-color textcolor3 #FFFFFF;
    @define-color iconcolor #FFFFFF;
  '';

  ml4w-opaque-dark = mkVariant "dark" ''
    @define-color backgroundlight @color8;
    @define-color backgrounddark #FFFFFF;
    @define-color workspacesbackground1 @color8;
    @define-color workspacesbackground2 #FFFFFF;
    @define-color bordercolor @color8;
    @define-color textcolor1 @color8;
    @define-color textcolor2 #FFFFFF;
    @define-color textcolor3 #FFFFFF;
    @define-color iconcolor @color8;
  '';

  ml4w-opaque-light = mkVariant "light" ''
    @define-color backgroundlight #FFFFFF;
    @define-color backgrounddark @color11;
    @define-color workspacesbackground1 #FFFFFF;
    @define-color workspacesbackground2 @color11;
    @define-color bordercolor #FFFFFF;
    @define-color textcolor1 #FFFFFF;
    @define-color textcolor2 @color11;
    @define-color textcolor3 #FFFFFF;
    @define-color iconcolor #FFFFFF;
  '';

  ml4w-opaque-mixed = mkVariant "mixed" ''
    @define-color backgroundlight @color8;
    @define-color backgrounddark #FFFFFF;
    @define-color workspacesbackground1 @color8;
    @define-color workspacesbackground2 #FFFFFF;
    @define-color bordercolor @color8;
    @define-color textcolor1 @color8;
    @define-color textcolor2 #FFFFFF;
    @define-color textcolor3 #FFFFFF;
    @define-color iconcolor #FFFFFF;
  '';

  ml4w-opaque-white = mkVariant "white" ''
    @define-color backgroundlight #FFFFFF;
    @define-color backgrounddark #FFFFFF;
    @define-color workspacesbackground1 #FFFFFF;
    @define-color workspacesbackground2 #CCCCCC;
    @define-color bordercolor #FFFFFF;
    @define-color textcolor1 #000000;
    @define-color textcolor2 #000000;
    @define-color textcolor3 #000000;
    @define-color iconcolor #FFFFFF;
  '';
}

