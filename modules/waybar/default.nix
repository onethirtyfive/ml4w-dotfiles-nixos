{ config, pkgs, lib, ... }:
let
  inherit (lib) types;
  cfg = config.ml4w.waybar;

  wrappedWaybar = pkgs.symlinkJoin {
    name = "ml4w-waybar-wrapped";

    paths =
         (with pkgs; [ waybar ])
      ++ (with cfg.defaults; [
        browser
        calculator
        chatgpt
        filemanager
        logout
        networkmanager
      ]);

    buildInputs = [ pkgs.makeWrapper ];

    postBuild = ''
      wrapProgram $out/bin/waybar
    '';
  };
in {
  # # Reload Waybar
  # setsid $HOME/dotfiles/waybar/launch.sh 1>/dev/null 2>&1 &
  options = {
    ml4w.waybar = {
      enable = lib.mkEnableOption "ML4W: Waybar";

      defaults = {
        browser = lib.mkOption {
          type = types.package;
          default = let
            inherit (pkgs) firefox;
          in pkgs.writeShellScriptBin "ml4w-waybar-browser.sh" ''
            ${firefox}/bin/firefox
          '';
        };

        calculator = lib.mkOption {
          type = types.package;
          default = let
            inherit (pkgs) qalculate-gtk;
          in pkgs.writeShellScriptBin "ml4w-waybar-calculator.sh" ''
            ${qalculate-gtk}/bin/qalculate-gtk https://chat.openai.com
          '';
        };

        chatgpt = lib.mkOption {
          type = types.package;
          default = let
            inherit (pkgs) firefox;
          in pkgs.writeShellScriptBin "ml4w-waybar-chatgpt.sh" ''
            ${firefox}/bin/firefox https://chat.openai.com
          '';
        };

        filemanager = lib.mkOption {
          type = types.package;
          default = let
            inherit (pkgs.xfce) thunar;
          in pkgs.writeShellScriptBin "ml4w-waybar-filemanager.sh" ''
            ${thunar}/bin/thunar
          '';
        };

        logout = lib.mkOption {
          type = types.package;
          default = let
            inherit (pkgs) wlogout;
          in pkgs.writeShellScriptBin "ml4w-waybar-logout.sh" ''
            ${wlogout}/bin/wlogout
          '';
        };

        networkmanager = lib.mkOption {
          type = types.package;
          default = let
            inherit (pkgs) networkmanagerapplet;
          in pkgs.writeShellScriptBin "ml4w-waybar-networkmanager.sh" ''
            ${networkmanagerapplet}/bin/nm-connection-editor
          '';
        };
      };

      commonBaseSettings = lib.mkOption {
        type = types.attrs;
        default = import ./common-base-settings.nix { inherit config pkgs; };
      };

      theme = lib.mkOption {
        type = types.str;
        default = "ml4w-default";
        description = "Off-the-shelf ML4W theme name";
      };

      tweaks = {
        workspaces = lib.mkOption {
          type = types.int;
          default = 5;
          description = "Number of workspaces";
        };

        appslabel = lib.mkOption {
          type = types.str;
          default = "Apps";
          description = "Label for apps starter";
        };

        date = lib.mkOption {
          type = types.str;
          default = "{:%Y-%m-%d}";
          description = "Date format of clock module";
        };
      };

      # DRY?
      widgets = {
        bluetooth = (lib.mkEnableOption "Bluetooth waybar module") // { default = true; };
        chatgpt = (lib.mkEnableOption "ChatGPT waybar module") // { default = true; };
        idle-inhibitor = (lib.mkEnableOption "Idle-inhibiting waybar module") // { default = true; };
        network = (lib.mkEnableOption "Network waybar module") // { default = true; };
        swaylock = (lib.mkEnableOption "Swaylock waybar module") // { default = true; };
        systray = (lib.mkEnableOption "Systray waybar module") // { default = true; };
        taskbar = (lib.mkEnableOption "Taskbar waybar module") // { default = true; };
        tray = (lib.mkEnableOption "Tray waybar module") // { default = true; };
      };
    };
  };

  config = lib.mkIf (config.ml4w.enable && cfg.enable) {
    services.mpd = {
      enable = true;
      musicDirectory = "~/Music";
    };

    programs.waybar =
    let
      themes = import ./themes { inherit config pkgs lib; };
      enabledTheme = themes."${cfg.theme}";
    in {
      enable = true;
      package = wrappedWaybar;
      systemd.enable = true;
      inherit (enabledTheme) settings style;
    };
  };
}

