#  __  __           _       _
# |  \/  | ___   __| |_   _| | ___  ___
# | |\/| |/ _ \ / _` | | | | |/ _ \/ __|
# | |  | | (_) | (_| | |_| | |  __/\__ \
# |_|  |_|\___/ \__,_|\__,_|_|\___||___/
#
#
# by Stephan Raabe (2023)
# -----------------------------------------------------
{ config, pkgs, ... }:
let
  inherit (pkgs) lib;
  cfg = config.ml4w;
in {
  "hyprland/workspaces" = {
    on-click = "activate";
    active-only = false;
    all-outputs = true;
    format = "{}";
    format-icons = {
      urgent = "";
      active = "";
      default = "";
    };
    persistent-workspaces = {
      "*" = cfg.waybar.tweaks.workspaces;
    };
  };

  "wlr/taskbar" = {
    format = "{icon}";
    icon-size = 18;
    tooltip-format = "{title}";
    on-click = "activate";
    on-click-middle = "close";
    ignore-list = [
      "Alacritty"
    ];
    app_ids-mapping = {
      # "firefoxdeveloperedition" = "firefox-developer-edition";
    };
    rewrite = {
      "Firefox Web Browser" = "Firefox";
      "Foot Server" = "Terminal";
    };
  };

  "hyprland/window" = {
    rewrite = {
      "(.*) - Brave" = "$1";
      "(.*) - Chromium" = "$1";
      "(.*) - Brave Search" = "$1";
      "(.*) - Outlook" = "$1";
      "(.*) Microsoft Teams" = "$1";
    };
    separate-outputs = true;
  };

  # Youtube Subscriber Count
  # "custom/youtube" = {
  #     format = " {};
  #     exec = "python ~/private/youtube.py;
  #     restart-interval = 600;
  #     on-click = "chromium https:#studio.youtube.com";
  #     tooltip = false;
  # };

  "custom/cliphist" = {
    format = "";
    on-click = "sleep 0.1 && ml4w-cliphist.sh";
    on-click-right = "sleep 0.1 && ml4w-cliphist.sh d";
    on-click-middle = "sleep 0.1 && ml4w-cliphist.sh} w";
    tooltip = false;
  };

  # Updates Count
  # "custom/updates" = {
  #   format = "  {}";
  #   tooltip-format = "{}";
  #   escape = true;
  #   return-type = "json";
  #   exec = "~/dotfiles/scripts/updates.sh";
  #   restart-interval = 60;
  #   on-click = "alacritty -e ~/dotfiles/scripts/installupdates.sh";
  #   on-click-right = "~/dotfiles/.settings/software.sh";
  #   tooltip = false;
  # };

  "custom/wallpaper" = {
    format = "";
    on-click = "${cfg.scripts.wallpaper}/bin/ml4w-wallpaper.sh select";
    on-click-right = "${cfg.scripts.wallpaper}/bin/ml4w-wallpaper.sh";
    tooltip = false;
  };

  "custom/waybarthemes" = {
    format = "";
    on-click = "FIXME";
    tooltip = false;
  };

  "custom/settings" = {
    format = "";
    on-click = "alacritty --class dotfiles-floating -e ~/dotfiles/hypr/start-settings.sh";
    tooltip = false;
  };

  "custom/keybindings" = {
    format = "";
    on-click = "FIXME";
    tooltip = false;
  };

  "custom/filemanager" = {
    format = "";
    on-click = "${cfg.waybar.defaults.filemanager}/bin/ml4w-waybar-filemanager.sh";
    tooltip = false;
  };

  "custom/outlook" = {
    format = "";
    on-click = "FIXME --app=https:#outlook.office.com/mail/";
    tooltip = false;
  };

  "custom/teams" = {
    format = "";
    on-click = "FIXME --app=https:#teams.microsoft.com/go";
    tooltip = false;
  };

  "custom/browser" = {
    format = "";
    on-click = "${cfg.waybar.defaults.browser}/bin/ml4w-waybar-browser.sh";
    tooltip = false;
  };

  "custom/chatgpt" = {
    format = "";
    on-click = "${cfg.waybar.defaults.browser}/bin/ml4w-waybar-browser.sh --app=https:#chat.openai.com";
    tooltip = false;
  };

  "custom/calculator" = {
    format = "";
    on-click = "${cfg.waybar.defaults.calculator}/bin/ml4w-waybar-calculator.sh";
    tooltip = false;
  };

  # "custom/windowsvm" = {
  #   format = "";
  #   on-click = "~/dotfiles/scripts/launchvm.sh";
  #   tooltip = false;
  # };

  "custom/appmenu" = {
    format = cfg.waybar.tweaks.appslabel; # configurable
    on-click = "rofi -show drun -replace";
    on-click-right = "~/dotfiles/hypr/scripts/keybindings.sh";
    tooltip = false;
  };

  "custom/exit" = {
    format = "";
    on-click = "${cfg.waybar.defaults.logout}/bin/ml4w-waybar-logout.sh";
    tooltip = false;
  };

  keyboard-state = {
    numlock = true;
    capslock = true;
    format = "{name} {icon}";
    format-icons = {
      locked = "";
      unlocked = "";
    };
  };

  tray = {
    icon-size = 21;
    spacing = 10;
  };

  clock = {
    # "timezone" = "America/New_York"
    tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
    format-alt = cfg.waybar.tweaks.date; # optionable
  };

  "group/hardware" = {
    orientation = "inherit";

    drawer = {
      transition-duration = 300;
      children-class = "not-memory";
      transition-left-to-right = false;
    };

    "custom/system" = {
      format = "";
      tooltip = false;
    };

    disk = {
      interval = 30;
      format = "D {percentage_used}% ";
      path = "/";
      on-click = "alacritty -e htop";
    };

    cpu = {
      format = "/ C {usage}% ";
      on-click = "alacritty -e htop";
    };

    memory = {
      format = "/ M {}% ";
      on-click = "alacritty -e htop";
    };

    "hyprland/language" = {
      format = "/ K {short}";
    };

    # modules = [
    #   "custom/system"
    #   "disk"
    #   "cpu"
    #   "memory"
    #   "hyprland/language"
    # ];
  };

  "group/settings" = {
    orientation = "inherit";
    drawer = {
      transition-duration = 300;
      children-class = "not-memory";
      transition-left-to-right = false;
    };
    modules = [
      "custom/settings"
      "custom/waybarthemes"
      "custom/wallpaper"
    ];
  };

  "group/quicklinks" = {
    orientation = "horizontal";
    modules =
         (lib.optionals cfg.waybar.widgets.chatgpt [ "custom/chatgpt" ])
      ++ [ "custom/filemanager" "custom/browser" ];
  };

  network = {
    format = "{ifname}";
    format-wifi = "   {signalStrength}%";
    format-ethernet = "  {ifname}";
    format-disconnected = "Disconnected";
    tooltip-format = " {ifname} via {gwaddri}";
    tooltip-format-wifi = "  {ifname} @ {essid}\nIP = {ipaddr}\nStrength = {signalStrength}%\nFreq = {frequency}MHz\nUp = {bandwidthUpBits} Down = {bandwidthDownBits}";
    tooltip-format-ethernet = " {ifname}\nIP = {ipaddr}\n up = {bandwidthUpBits} down = {bandwidthDownBits}";
    tooltip-format-disconnected = "Disconnected";
    max-length = 50;
    on-click = "~/dotfiles/.settings/networkmanager.sh";
  };

  battery = {
    states = {
      # good = 95;
      warning = 30;
      critical = 15;
    };
    format = "{icon}   {capacity}%";
    format-charging = "  {capacity}%";
    format-plugged = "  {capacity}%";
    format-alt = "{icon}  {time}";
    # format-good = ""; # An empty format will hide the module
    # format-full = "";
    format-icons = [" " " " " " " " " "];
  };

  pulseaudio = {
    # scroll-step = 1; # %, can be a float
    format = "{icon} {volume}%";
    format-bluetooth = "{volume}% {icon} {format_source}";
    format-bluetooth-muted = " {icon} {format_source}";
    format-muted = " {format_source}";
    format-source = "{volume}% ";
    format-source-muted = "";
    format-icons = {
      headphone = "";
      hands-free = "";
      headset = "";
      phone = "";
      portable = "";
      car = "";
      default = ["" " " " "];
    };
    on-click = "pavucontrol";
  };

  bluetooth = {
    format = " {status}";
    format-disabled = "";
    format-off = "";
    interval = 30;
    on-click = "blueman-manager";
  };

  user = {
    format = "{user}";
    interval = 60;
    icon = false;
  };

  "idle_inhibitor" = {
    format = "{icon}";
    tooltip = true;
    format-icons = {
      activated = "";
      deactivated = "";
    };
    on-click-right = "swaylock";
  };
}

