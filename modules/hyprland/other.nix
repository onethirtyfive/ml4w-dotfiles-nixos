{ pkgs, ml4w-scripts, ... }:
let
  presets = {
    autostart = ''
      # exec-once = ~/dotfiles/hypr/scripts/xdg.sh
      # exec-once = /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
      exec-once = dunst
      exec-once = hyprctl setcursor Bibata-Modern-Ice 24
      exec-once = ~/dotfiles/gtk/gtk.sh
      exec-once = ~/dotfiles/hypr/scripts/lockscreentime.sh
      # Load network manager applet
      # START NM APPLET
      # exec-once = nm-applet --indicator
      # END NM APPLET
      exec-once = wl-paste --watch cliphist store
      exec-once = swww query || swww init
      exec-once = ~/dotfiles/hypr/scripts/wallpaper.sh init
    '';

    # TBD: Better way to do this?
    custom = ''
      # Add your additional Hyprland configurations here
      #
      # This is an additional key binding
      # bind = $mainMod CTRL, up, workspace, empty
      #
      # Example for xwayland
      # xwayland {
      #   force_zero_scaling = true
      # }
    '';

    keybindings = ''
      # SUPER KEY
      $mainMod = SUPER

      # Applications
      bind = $mainMod, RETURN, exec, alacritty
      bind = $mainMod, B, exec, ~/dotfiles/.settings/browser.sh

      # Windows
      bind = $mainMod, Q, killactive
      bind = $mainMod, F, fullscreen
      bind = $mainMod, E, exec, ${ml4w-scripts.filemanager}
      bind = $mainMod, T, togglefloating
      bind = $mainMod SHIFT, T, exec, ${ml4w-scripts.toggleallfloat}
      # bind = $mainMod, SHIFT, J, togglesplit # changed to accommodate vim bindings
      bind = $mainMod, left, movefocus, l
      bind = $mainMod, right, movefocus, r
      bind = $mainMod, up, movefocus, u
      bind = $mainMod, down, movefocus, d
      bindm = $mainMod, mouse:272, movewindow
      bindm = $mainMod, mouse:273, resizewindow
      bind = $mainMod SHIFT, right, resizeactive, 100 0
      bind = $mainMod SHIFT, left, resizeactive, -100 0
      bind = $mainMod SHIFT, up, resizeactive, 0 -100
      bind = $mainMod SHIFT, down, resizeactive, 0 100

      # Vim-style move binds
      bind = $mainMod, H, movefocus, l
      bind = $mainMod, J, movefocus, d
      bind = $mainMod, K, movefocus, u
      bind = $mainMod, L, movefocus, r
      bind = $mainMod ALT, H, resizeactive, -100 0
      bind = $mainMod ALT, J, resizeactive, 0 100
      bind = $mainMod ALT, K, resizeactive, 0 -100
      bind = $mainMod ALT, L, resizeactive, 100 0

      # Actions
      bind = $mainMod, PRINT, exec, ${ml4w-scripts.screenshot}
      bind = $mainMod CTRL, Q, exec, wlogout
      bind = $mainMod SHIFT, W, exec, ${ml4w-scripts.wallpaper}
      bind = $mainMod CTRL, W, exec, ${ml4w-scripts.wallpaper} select
      bind = $mainMod CTRL, RETURN, exec, rofi -show drun
      bind = $mainMod CTRL, H, exec, ~/dotfiles/hypr/scripts/keybindings.sh
      bind = $mainMod SHIFT, B, exec, ~/dotfiles/waybar/launch.sh
      bind = $mainMod SHIFT, R, exec, ~/dotfiles/hypr/scripts/loadconfig.sh
      bind = $mainMod CTRL, F, exec, ~/dotfiles/scripts/filemanager.sh
      bind = $mainMod CTRL, C, exec, ~/dotfiles/scripts/cliphist.sh
      bind = $mainMod, V, exec, ~/dotfiles/scripts/cliphist.sh
      bind = $mainMod CTRL, T, exec, ~/dotfiles/waybar/themeswitcher.sh
      bind = $mainMod CTRL, S, exec, alacritty --class dotfiles-floating -e ~/dotfiles/hypr/start-settings.sh

      # Workspaces
      bind = $mainMod, 1, workspace, 1
      bind = $mainMod, 2, workspace, 2
      bind = $mainMod, 3, workspace, 3
      bind = $mainMod, 4, workspace, 4
      bind = $mainMod, 5, workspace, 5
      bind = $mainMod, 6, workspace, 6
      bind = $mainMod, 7, workspace, 7
      bind = $mainMod, 8, workspace, 8
      bind = $mainMod, 9, workspace, 9
      bind = $mainMod, 0, workspace, 10
      bind = $mainMod SHIFT, 1, movetoworkspace, 1
      bind = $mainMod SHIFT, 2, movetoworkspace, 2
      bind = $mainMod SHIFT, 3, movetoworkspace, 3
      bind = $mainMod SHIFT, 4, movetoworkspace, 4
      bind = $mainMod SHIFT, 5, movetoworkspace, 5
      bind = $mainMod SHIFT, 6, movetoworkspace, 6
      bind = $mainMod SHIFT, 7, movetoworkspace, 7
      bind = $mainMod SHIFT, 8, movetoworkspace, 8
      bind = $mainMod SHIFT, 9, movetoworkspace, 9
      bind = $mainMod SHIFT, 0, movetoworkspace, 10
      bind = $mainMod, mouse_down, workspace, e+1 # bad for split keyboards
      bind = $mainMod, mouse_up, workspace, e-1 # bad for split keyboards
      bind = $mainMod CTRL, down, workspace, empty

      # Vim-style workspace binds
      bind = $mainMod, SHIFT, H, moveactive, l
      bind = $mainMod, SHIFT, J, moveactive, d
      bind = $mainMod, SHIFT, K, moveactive, u
      bind = $mainMod, SHIFT, L, moveactive, r

      # Fn keys
      bind = , XF86MonBrightnessUp, exec, brightnessctl -q s +10%
      bind = , XF86MonBrightnessDown, exec, brightnessctl -q s 10%-
      bind = , XF86AudioRaiseVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ +5%
      bind = , XF86AudioLowerVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ -5%
      bind = , XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
      bind = , XF86AudioPlay, exec, playerctl play-pause
      bind = , XF86AudioPause, exec, playerctl pause
      bind = , XF86AudioNext, exec, playerctl next
      bind = , XF86AudioPrev, exec, playerctl previous
      bind = , XF86AudioMicMute, exec, pactl set-source-mute @DEFAULT_SOURCE@ toggle
      bind = , XF86Calculator, exec, qalculate-gtk
      bind = , XF86Lock, exec, swaylock
      bind = , XF86Tools, exec, alacritty --class dotfiles-floating -e ~/dotfiles/hypr/settings/settings.sh

      # Passthrough SUPER KEY to Virtual Machine
      bind = $mainMod, P, submap, passthru
      submap = passthru
      bind = SUPER, Escape, submap, reset
      submap = reset
    '';


    keyboard = ''
      input {
          kb_layout = us
          kb_variant =
          kb_model =
          kb_options = terminate:ctrl_alt_bksp,caps:escape

          follow_mouse = 2
          touchpad {
              natural_scroll = false
              disable_while_typing = true
              drag_lock = true
              tap-to-click = true
          }

          sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
          float_switch_override_focus = 2

          repeat_rate = 60
          repeat_delay = 375
      }
    '';

    layout = ''
      dwindle {
          pseudotile = true
          preserve_split = true
      }

      master {
          new_is_master = true
      }

      gestures {
          workspace_swipe = false
      }

      device:epic-mouse-v1 {
          sensitivity = -0.5
      }
    '';

    misc = ''
      misc {
          disable_hyprland_logo = true
          disable_splash_rendering = true
      }
    '';

    ml4w = ''
      windowrulev2 = float,class:(ml4w-about)
      windowrulev2 = size 60% 60%,class:(ml4w-about)
      windowrulev2 = center,class:(ml4w-about)

      windowrulev2 = float,class:(dotfiles-floating)
      windowrulev2 = size 60% 60%,class:(dotfiles-floating)
      windowrulev2 = center,class:(dotfiles-floating)
    '';


    # Merged in from prior standalone.
    windowrules = ''
      windowrule = tile,^(Microsoft-edge)$
      windowrule = tile,^(Brave-browser)$
      windowrule = tile,^(Chromium)$
      windowrule = float,^(pavucontrol)$
      windowrule = float,^(blueman-manager)$
      windowrule = float,^(nm-connection-editor)$
    '';
  };
in builtins.mapAttrs
  (name: value:
    let
      baked = "ml4w-hyprland-decorations-preset-${name}.conf";
    in pkgs.writeText baked value
  )
  presets

