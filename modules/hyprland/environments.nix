{ pkgs, ... }:
let
  presets = {
    kvm = ''
      env = XCURSOR_SIZE,24
      env = QT_QPA_PLATFORM,wayland
      env = GTK_THEME,Adwaita:dark
      env = WLR_NO_HARDWARE_CURSORS, 1
      env = WLR_RENDERER_ALLOW_SOFTWARE, 1
    '';

    default = ''
      env = XCURSOR_SIZE,24
      env = QT_QPA_PLATFORM,wayland
      env = GTK_THEME,Adwaita:dark
    '';
  };
in builtins.mapAttrs
  (name: value:
    let
      baked = "ml4w-hyprland-environments-preset-${name}.conf";
    in pkgs.writeText baked value
  )
  presets

