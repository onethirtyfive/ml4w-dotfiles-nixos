{ pkgs, ... }:
let
  presets = {
    "1440x1080" = "monitor=,1440x1080,auto,1";
    "1920x1080" = "monitor=,1920x1080,auto,1";
    "2560x1440" = "monitor=,2560x1440,auto,1";
    "2560x1440@120" = "monitor=,2560x1440@120,auto,1";
    default = "monitor=,preferred,auto,1";
  };
in builtins.mapAttrs
  (name: value:
    let
      baked = "ml4w-hyprland-monitors-preset-${name}.conf";
    in pkgs.writeText baked value
  )
  presets

