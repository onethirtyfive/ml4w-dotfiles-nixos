{ pkgs, ... }:
let
  presets = rec {
    border-1 = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 1
          col.active_border = $color11
          col.inactive_border = rgba(ffffffff)
          layout = dwindle
      }
    '';

    border-1-reverse = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 1
          col.active_border = rgba(ffffffff)
          col.inactive_border = $color11
          layout = dwindle
      }
    '';

    border-2 = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 2
          col.active_border = $color11
          col.inactive_border = rgba(ffffffff)
          layout = dwindle
      }
    '';

    border-2-reverse = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 2
          col.active_border = rgba(ffffffff)
          col.inactive_border = $color11
          layout = dwindle
      }
    '';

    border-3 = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 3
          col.active_border = $color11
          col.inactive_border = rgba(ffffffff)
          layout = dwindle
      }
    '';

    border-3-reverse = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 3
          col.active_border = rgba(ffffffff)
          col.inactive_border = $color11
          layout = dwindle
      }
    '';

    border-4 = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 4
          col.active_border = $color11
          col.inactive_border = rgba(ffffffff)
          layout = dwindle
      }
    '';

    border-4-reverse = ''
      general {
          gaps_in = 10
          gaps_out = 14
          border_size = 4
          col.active_border = rgba(ffffffff)
          col.inactive_border = $color11
          layout = dwindle
      }
    '';

    default = border-3;
  };
in builtins.mapAttrs
  (name: value:
    let
      baked = "ml4w-hyprland-windowing-preset-${name}.conf";
    in pkgs.writeText baked value
  )
  presets

