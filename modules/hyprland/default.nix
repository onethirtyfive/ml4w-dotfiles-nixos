{ config, pkgs, lib, ... }:
let
  ml4w-scripts = config.ml4w.scripts;
  cfg = config.ml4w.hyprland;
in {
  # # Reload Waybar
  # setsid $HOME/dotfiles/waybar/launch.sh 1>/dev/null 2>&1 &
  options = {
    ml4w.hyprland = with lib; {
      enable = mkEnableOption "ML4W: Hyprland";
      keybindings = mkOption {
        type = types.str;
        default = "default";
        description = "ML4W key bindings";
      };

      presets = {
        animations = mkOption {
          type = types.str;
          default = "default";
          description = "ML4W animations preset name";
        };

        decorations = mkOption {
          type = types.str;
          default = "default";
          description = "ML4W decorations preset name";
        };

        environments = mkOption {
          type = types.str;
          default = "default";
          description = "ML4W environments preset name";
        };

        monitors = mkOption {
          type = types.str;
          default = "default";
          description = "ML4W monitor settings";
        };

        windowrules = mkOption {
          type = types.str;
          default = "default";
          description = "ML4W window rules";
        };

        windowing = mkOption {
          type = types.str;
          default = "default";
          description = "Off-the-shelf ML4W window theme name";
        };
      };

      defaults = {
        # echo "Define the start command to start the browser (Default: chromium)."
        #
        # # Define File
        # targetFile="$HOME/dotfiles/.settings/browser.sh"
        # browser = {
        # };
      };

    };
  };

  config = lib.mkIf (config.ml4w.enable && cfg.enable) {
    programs.pywal.enable = true;

    dconf.settings = {
      "org/gnome/desktop/interface" = {
        icon-theme = "Papirus-Dark";
        cursor-theme = "Bibata-Modern-Ice";
        font-name = "Cantarell 11";
        color-scheme = "prefer-dark";
      };
    };

    services.dunst = {
      enable = true;
      settings = {
        global = {
          monitor = 0;
          follow = "none";
          width = 300;
          height = 300;
          origin = "top-center";
          offset = "0x30";
          scale = 0;
          notification_limit = 20;

          progress_bar = true;
          progress_bar_height = 10;
          progress_bar_frame_width = 1;
          progress_bar_min_width = 150;
          progress_bar_max_width = 300;
          progress_bar_corner_radius = 10;

          icon_corner_radius = 0;
          indicate_hidden = true;
          transparency = 30;
          separator_height = 2;

          padding = 8;
          horizontal_padding = 8;
          text_icon_padding = 0;

          frame_width = 3;
          frame_color = "#ffffff";
          gap_size = 0;
          separator_color = "frame";
          sort = "yes";
          font = "Fira Sans Semibold 11";
          line_height = 3;
          markup = "full";
          format = "<b>%s</b>\\n%b";
          alignment = "left";
          vertical_alignment = "center";

          show_age_threshold = 60;
          ellipsize = "middle";
          ignore_newline = false;

          stack_duplicates = true;
          hide_duplicate_count = false;
          show_indicators = true;

          enable_recursive_icon_lookup = true;
          icon_theme = "Adwaita";
          icon_position = "left";

          min_icon_size = 32;
          max_icon_size = 128;

          # nixify:
          # icon_path = /usr/share/icons/gnome/16x16/status/:/usr/share/icons/gnome/16x16/devices/

          sticky_history = true;
          history_length = 20;

          # nixify
          # dmenu = /usr/bin/dmenu -p dunst:
          # browser = /usr/bin/xdg-open

          always_run_script = true;

          # Define the title of the windows spawned by dunst
          title = "Dunst";
          class = "Dunst";

          corner_radius = 10;
          ignore_dbusclose = false;
          force_xwayland = false;
          force_xinerama = false;
          mouse_left_click = "close_current";
          mouse_middle_click = "do_action, close_current";
          mouse_right_click = "close_all";
        };

        experimental = {
          per_monitor_dpi = false;
        };

        urgency_low = {
          background = "#00000070";
          foreground = "#888888";
          timeout = 6;
        };

        urgency_normal = {
          background = "#00000070";
          foreground = "#ffffff";
          timeout = 6;
        };

        urgency_critical = {
          background = "#90000070";
          foreground = "#ffffff";
          frame_color = "#ffffff";
          timeout = 6;
        };
      };
    };

    home.activation.ml4w = let
      wallpapers = config.ml4w.wallpapers;
      cachedir = config.xdg.cacheHome;
      configdir = config.xdg.configHome;
      inherit (pkgs) pywal gnused;

      waybarConfigdir = "${configdir}/waybar";
      walCachedir = "${cachedir}/wal";
    in lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      set +eux

      # Create cache file if not exists
      cache_file="${cachedir}/current_wallpaper"
      if [ ! -f $cache_file ] ;then
          touch $cache_file
          echo "${wallpapers}/default.jpg" > "$cache_file"
      fi

      current_wallpaper=$(cat "$cache_file")
      $DRY_RUN_CMD ${pywal}/bin/wal -q -i "$current_wallpaper"

      # wouldn't it just be easier to roll our own systemd service and kick it off
      # with a custom config file?
      echo "${waybarConfigdir}/styles.css"
      set +x
      $DRY_RUN_CMD ${gnused}/bin/sed -n '/\/* BEGIN STYLES \*\//,$p' "${waybarConfigdir}/style.css" | cat "${walCachedir}/colors-waybar.css" - > "${waybarConfigdir}/style.css.tmp" && mv "${waybarConfigdir}/style.css.tmp" "${waybarConfigdir}/style.css"

      echo "Done"
    '';

    home.packages = with pkgs; [
      bibata-cursors
      cantarell-fonts
      papirus-icon-theme
      wlogout
    ];

    xdg.desktopPortals = {
      xdgOpenUsePortal = true;
      enable = true;
      portals = let useIn = [ "Hyprland" ];
      in {
        hyprland = {
          package = pkgs.xdg-desktop-portal-hyprland;
          inherit useIn;
        };
        # gtk = {
        #   package = pkgs.libsForQt5.xdg-desktop-portal-gtk;
        #   interfaces = [ "org.freedesktop.impl.portal.FileChooser" ];
        #   inherit useIn;
        # };
      };
    };

    wayland.windowManager.hyprland = {
      settings = {
        exec-once = [
          "hyprctl setcursor Bibata-Modern-Ice 24"
          "wl-paste --watch cliphist store"
          "swww query || swww init"
        ];
      };
      extraConfig = let
        derivedSources = {
          animations = (import ./animations.nix { inherit pkgs; })."${cfg.presets.animations}";
          decorations = (import ./decorations.nix { inherit pkgs; })."${cfg.presets.decorations}";
          environments = (import ./environments.nix { inherit pkgs; })."${cfg.presets.environments}";
          monitors = (import ./monitors.nix { inherit pkgs; })."${cfg.presets.monitors}";
          windowing = (import ./windowing.nix { inherit pkgs; })."${cfg.presets.windowing}";
        } // (import ./other.nix { inherit pkgs ml4w-scripts; });
      in ''
        # ----------------------------------------------------- Monitor
        # -----------------------------------------------------
        # source = ~/dotfiles/hypr/conf/monitor.conf

        # -----------------------------------------------------
        # Autostart
        # -----------------------------------------------------
        # source = ~/dotfiles/hypr/conf/autostart.conf

        # -----------------------------------------------------
        # Environment
        # -----------------------------------------------------
        source = ${derivedSources.environments}

        # -----------------------------------------------------
        # Keyboard
        # -----------------------------------------------------
        source = ${derivedSources.keyboard}

        # -----------------------------------------------------
        # Load pywal color file
        # -----------------------------------------------------
        # source = ~/.cache/wal/colors-hyprland.conf

        # -----------------------------------------------------
        # Load configuration files
        # -----------------------------------------------------
        source = ${derivedSources.windowing}
        source = ${derivedSources.decorations}
        source = ${derivedSources.layout}
        source = ${derivedSources.misc}
        # source = ~/dotfiles/hypr/conf/keybinding.conf
        source = ${derivedSources.windowrules}

        # -----------------------------------------------------
        # Animation
        # -----------------------------------------------------
        source = ${derivedSources.animations}

        # -----------------------------------------------------
        # Custom
        # -----------------------------------------------------
        # source = ~/dotfiles/hypr/conf/custom.conf

        # -----------------------------------------------------
        # ML4W
        # -----------------------------------------------------
        source = ${derivedSources.ml4w}

        # -----------------------------------------------------
        # Environment for xdg-desktop-portal-hyprland
        # -----------------------------------------------------
        # exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
      '';
    };
  };
}

