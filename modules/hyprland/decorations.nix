{ pkgs, ... }:
let
  presets = {
    no-rounding-more-blur = ''
      decoration {
          rounding = 0
          blur {
              enabled = true
              size = 12
              passes = 6
              new_optimizations = on
              ignore_opacity = true
              xray = true
              # blurls = waybar
          }
          active_opacity = 1.0
          inactive_opacity = 0.6
          fullscreen_opacity = 1.0

          drop_shadow = true
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';

    no-rounding = ''
      decoration {
          rounding = 0
          blur {
              enabled = true
              size = 6
              passes = 2
              new_optimizations = on
              ignore_opacity = true
              xray = true
              # blurls = waybar
          }
          active_opacity = 1.0
          inactive_opacity = 0.8
          fullscreen_opacity = 1.0

          drop_shadow = true
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';

    rounding-all-blur-no-shadows = ''
      decoration {
          rounding = 10
          blur {
              enabled = true
              size = 12
              passes = 4
              new_optimizations = on
              ignore_opacity = true
              xray = true
              blurls = waybar
          }
          active_opacity = 0.9
          inactive_opacity = 0.6
          fullscreen_opacity = 0.9

          drop_shadow = false
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';

    rounding-all-blur = ''
      decoration {
          rounding = 10
          blur {
              enabled = true
              size = 12
              passes = 4
              new_optimizations = on
              ignore_opacity = true
              xray = true
              blurls = waybar
          }
          active_opacity = 0.9
          inactive_opacity = 0.6
          fullscreen_opacity = 0.9

          drop_shadow = true
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';

    rounding-more-blur = ''
      decoration {
          rounding = 10
          blur {
              enabled = true
              size = 12
              passes = 6
              new_optimizations = on
              ignore_opacity = true
              xray = true
              # blurls = waybar
          }
          active_opacity = 1.0
          inactive_opacity = 0.6
          fullscreen_opacity = 1.0

          drop_shadow = true
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';

    rounding-opaque = ''
      decoration {
          rounding = 10
          blur {
              enabled = false
              size = 6
              passes = 2
              new_optimizations = on
              ignore_opacity = true
              xray = true
              # blurls = waybar
          }
          active_opacity = 1.0
          inactive_opacity = 1.0
          fullscreen_opacity = 1.0

          drop_shadow = true
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';

    rounding = ''
      decoration {
          rounding = 10
          blur {
              enabled = true
              size = 6
              passes = 2
              new_optimizations = on
              ignore_opacity = true
              xray = true
              # blurls = waybar
          }
          active_opacity = 1.0
          inactive_opacity = 0.8
          fullscreen_opacity = 1.0

          drop_shadow = true
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';

    default = ''
      decoration {
          rounding = 10
          blur {
              enabled = true
              size = 6
              passes = 2
              new_optimizations = on
              ignore_opacity = true
              xray = true
              # blurls = waybar
          }
          active_opacity = 1.0
          inactive_opacity = 0.8
          fullscreen_opacity = 1.0

          drop_shadow = true
          shadow_range = 30
          shadow_render_power = 3
          col.shadow = 0x66000000
      }
    '';
  };
in builtins.mapAttrs
  (name: value:
    let
      baked = "ml4w-hyprland-decorations-preset-${name}.conf";
    in pkgs.writeText baked value
  )
  presets

