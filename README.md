**This project is on pause, as I got too busy.**

# ML4W Dotfiles: NixOS Port

This is an as-faithful-as-I-can port of Stephan Raabe's excellent [ML4W dotfiles](https://gitlab.com/stephan-raabe/dotfiles)
project, but re-expressed for NixOS as a thin "meta-module" configuring Home
Manager to function like ML4W-dotfiles do over on Arch.

Important: this port will initially (and maybe only ever) target Hyprland.

It will initially have parity with v2.7 of ML4W dotfiles, circa Dec 2023.

The use of this name is only for discoverability--it does not imply any
endorsement from the original product author.

## State of Development

Early. Like, "unusable without a bit more glue" early.

Oops. I let the cat out of the bag over on youtube before this was fully baked.
But... I'd rather publish "wip" code--even stuff that doesn't work--than leave
my fellow Nix fanboys hanging. So here are... a bunch of files.

It's doable, and progressing.

## How Do I Use It?

You don't right now. :( I need to write up a `flake.nix` and dogfood this repo
before I'm in a position to write a how-to. But my own computers aren't usable
until I do, so--I'm motivated! Stay tuned!

This is literally a `cp -r` of where it originally lived.

## What's working?

Forthcoming. Several things are working well, though, like wallpaper change
animations! I'd rather make it work than document how it doesn't work.

