{ config, pkgs, ... }:
let
  inherit (pkgs) callPackage;

  rofi = config.programs.rofi.finalPackage; # as configured via hm
in {
  calculator = callPackage ./calculator.nix { inherit rofi; };
  # checkplatform = callPackage ./checkplatform.nix {};
  cliphist = callPackage ./cliphist.nix { inherit config rofi; };
  # figlet = callPackage ./figlet.nix {};
  # filemanager = callPackage ./filemanager.nix {};
  fontsearch = callPackage ./fontsearch.nix {};
  templates = callPackage ./templates.nix { inherit rofi; };
}
